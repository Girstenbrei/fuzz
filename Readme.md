# Analysis of FairFuzz
This repo contains a university assignment for LMU. This assignment was done during my masters thesis in computer science as final project for the course [Fuzz Testing](https://www.sosy-lab.org/Teaching/2019-WS-Seminar-Fuzz-Testing/)

## Source code
All source code to test examples and run afl and FairFuzz in containers can be found in the `src` directory.

## Latex
The tex file contains the latex source code for this project.