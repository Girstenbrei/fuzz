\documentclass[a4paper,11pt]{article}

% Macht evtl unter Windows Probleme?
\usepackage[utf8]{inputenc}

% weitere Pakete hier
\usepackage{amsmath}
\usepackage{todonotes}
\renewcommand\todo[1]{}
\usepackage[printonlyused,nohyperlinks,nolist]{acronym}
\usepackage{graphicx}
\usepackage{wrapfig}
\usepackage{booktabs}
\usepackage[toc,page]{appendix}
\usepackage[bottom, multiple]{footmisc}

% Nice format code listings
\usepackage{color}
% \definecolor{mygreen}{rgb}{0,0.6,0}
\definecolor{mygreen}{HTML}{00998c}
\definecolor{mygray}{rgb}{0.5,0.5,0.5}
\definecolor{mymauve}{rgb}{0.58,0,0.82}
\definecolor{string}{HTML}{bc6c25}
\definecolor{keywords}{HTML}{005999}
\usepackage{listings}
\lstset{
    language=C,
    numbers=left,
    basicstyle=\sffamily\footnotesize,
    commentstyle=\color{mygreen},
    keywordstyle=\bfseries\color{keywords},
    numberstyle=\tiny\color{mygray}, % the style that is used for the line-numbers
    rulecolor=\color{black},
    stringstyle=\color{string},
}
\graphicspath{ {./images/} }
% Referenzen schön formatieren, Kommandos
% \cite  -> [x]
% \citet -> Name [x]
\usepackage[sort&compress,numbers]{natbib}
\setlength{\bibsep}{2pt plus 0.3ex}
\renewcommand*{\bibfont}{\small}
\makeatletter
\def\NAT@spacechar{~}% NEW
\makeatother

% Klickbare Links im Dokument
\usepackage[hyperfootnotes=false,colorlinks]{hyperref} % muss als vorletztes Paket eingebunden werden
% Evtl auskommentieren
 
% Querverweise mit \cref und \Cref
\usepackage[noabbrev]{cleveref} % muss als letztes Paket eingebunden werden

\begin{acronym}
    \acro{CGGF}{Coverage Guided Grey-box Fuzzing}
    \acro{AFL}{American Fuzzy Lop}
\end{acronym}

\author{Christoph Girstenbrei\\Student ID:\ 10769022\\\texttt{christoph.girstenbrei@lmu.de}}
\title{\bf FairFuzz}

\begin{document}
\maketitle

\begin{center}
Masterseminar Fuzz Testing
\end{center}

\begin{abstract}
This paper is an analysis of the work done by \citet{Lemieux2018} on enhancing \ac{CGGF}. It gives insight into the concepts behind and workings of the tool FairFuzz. It shows how targeting rare branches and masking modifications of the input results in faster fuzzing success and more complete input selection. The solutions of FairFuzz are compared to different tools like Driller or VeriFuzz and different concepts like symbolic execution to reveal their advantages as well as disadvantages.
\end{abstract}

\section{Introduction}
% Present complexity concepts / introduce cggf
Fuzz testing has risen in popularity and tools like \ac{AFL} have found bugs in well known applications like Mozilla Firefox and OpenSSL\cite{Zalewski2014,Nils2015}. In order to enable a broad audience to use these tools and as well as to speed up detection in large programs, the tools need to be optimized. \todo{Cite need for speed optimizing. E.g.\ tools taking hours}

Especially \ac{CGGF} has gained users with tools like \ac{AFL}, which has over 100 forks and more than 800 Stars on Github by the time writing this. \ac{AFL} requires only three preconditions: the instrumented source program, a start input file and a way to provide this input to the source program.
Everything else can be done by \ac{AFL} without supervision. This enables one to use computing power to find bugs in an automated way. Thereby not only developers can find bugs within their own software, it also makes it possible for external entities to do so.
In contrast to e.g.\ writing integration tests, requiring intrinsic knowledge of the software under test, running \ac{CGGF} is much easier and can be done by many more entities, with the computing power present being the limiting factor.

Providing more computational power is one solution to quicker fuzzing results, Microsoft even arguing in 2010 ``that fuzzing in the cloud will revolutionize security testing''\cite{Godefroid2010}. But making fuzzers themselves more efficient will result in an advantage across the board, enabling faster and cheaper bug discovery for a more broad audience.

One tool attempting to do so is FairFuzz. It is built on top of \ac{AFL} and attempts to enhance the bug detection performance of the same.
This is done by speeding up the detection of interesting parts of a piece of software using the concept of rare branches as well as preventing \ac{AFL} from performing unnecessary computations by preventing mutations in the input on necessary sections of it.

\section{Background Information}
% \subsection{Identifying bottlenecks in coverage guided greybox fuzzing}
% connect better guessing with faster bug detection
Not considering physical limitations or implementation details, the minimum phases executed by a fuzzer using \ac{CGGF} are selecting inputs, mutating inputs, running them through the piece of software under test and interpreting the results of this run. Some fuzzers may add additional phases to this pipeline but every \ac{CGGF} fuzzer executes it in repeat to gain information. Therefore this pipelines performance is relevant and optimizing it for speed is valuable though some phases may be more easily optimized than others. A simplified representation of the time taken to find a specific bug and all bugs within a program can be seen in \ref{math:time_bug}.

\begin{wrapfigure}{R}{0.6\textwidth}
    \centering
    \begin{align*}
        time\left(bug_{x}\right) &= n_{iter}\,\cdot\,\left(t_{sel}+t_{mut}+t_{run}+t_{check}\right) \\
        time\left(Bugs\right) &= \underset{b\in Bugs}{\sum}\left(time\left(b\right)\right) 
    \end{align*}
    \caption{Time to find bugs with n iterations per bug, times for selection, mutation, a run and check of the results.}\label{math:time_bug}
\end{wrapfigure}

The last phase of interpreting the results (\(t_{check}\)) is concerned with correctness rather than speed. But optimization here will only improve the type of bugs found rather than overall execution speed. The second to last one (\(t_{run}\)), running inputs through the piece of software, is mostly dependent on the software under test. Speeding up this process may be possible but largely dependent on the program under test. Additionally, both of these phases yield only linear time savings until one specific bug is found. Improving both of them does not change the number of iterations until the bug is found. This results in those phases being not as rewarding considering optimization.

This leaves the input selection (\(t_{sel}\)) and input mutation (\(t_{mut}\)) stages for optimization.
Without information about conditionals within the program under test, the challenge for fuzzers is how to produce the most appropriate inputs within these two stages. The better the fuzzer can ``guess'' these inputs, the faster it will detect bugs. The measurement of better in this case is given through the coverage metric.

The first two phases are especially interesting, as producing better inputs has direct influence on the number of iterations. This means, better selection and mutation can result in a direct reduction of \(n_{iter}\) for a specific bug. For overall bug detection, this results in exponential savings on the time to detect all bugs. Therefore, even small increases in (\(t_{sel}\)) and (\(t_{mut}\)) may be beneficial if the benefit is a lower \(n_{iter}\).

\subsection{\ac{AFL}s approach}
%Already existing approach by afl
\ac{AFL} already incorporates strategies to execute the aforementioned phases.\@

One central concept to this is the one of branch coverage. Inputs are evaluated by \ac{AFL} according to their achieved branch coverage. This is measured by first instrumenting the code under observation. To do so, every basic block is given a unique ID. While executing with a specific input, one can then associate the IDs passed by and the current input with one another to record which blocks where executed. Given this information, it is possible to evaluate different inputs against one another. Then only inputs associated with IDs not already hit by other ones are saved as they execute code not already explored.\cite[2.2 AFL Coverage Calculation]{Lemieux2018}

To select appropriate input, \ac{AFL} uses generation and trimming. In generation of new inputs, the mutation stage adds new elements to the input queue. In order to keep single inputs from growing to large, these are regularly trimmed. This increases speed and is done by removing ``blocks of data with variable length and step-over'' but ensuring those inputs still hit the same coverage points.\cite{afldocs}

To generate new inputs, \ac{AFL} has two different mutation strategies: deterministic and havoc. The deterministic strategy includes bit flips, addition/subtraction of small integers and ``insertion of known interesting integers''. The non-deterministic stage (havoc) \ac{AFL} includes stacked bit flips, insertions, deletions, arithmetics and splicing of different test cases.\cite[6 Fuzzing Strategies]{afldocs}

These combined allow for \ac{AFL} to generate new input but do not incorporate any prioritization of inputs or mutation.

\subsection{\ac{AFL}s limits}
% introduce problems there in afl (masks + input selection)
The advantage of these methods is, that they are not biased towards producing specific input and therefore can produce any input expected by any program. The only condition is, that it is possible for \ac{AFL} to connect the program under test to this input. This can be done by pointing \ac{AFL} to a file read by the program or piping to its stdin.

The drawback of this approach inherently leads to a very large search space for the input. And as software like protocol parsers, image converters or document processors require a very structured input, this approach falls short in producing those in an efficient manor. The authors of \ac{AFL} themselves state that e.g. flipping a single bit in an input will result in approx. ``70 new paths per one million generated inputs''\cite{lcamtuf2014} on average. This highlights the importance of effective input generation.

In the case of input selection, generating new inputs from those already exploring a well known part of the program may not be as beneficial as producing inputs for rarely covered parts.

\begin{minipage}[b]{\linewidth}
\begin{lstlisting}[caption={Example of nested comparisons},label=lst:comp]
// [ input read into buf ]
if (buf[0] == 'a'){
  if (buf[1] == 'b'){
    if (buf[2] == 'c'){
      if (buf[3] == 'd'){
        int b = 3 / (div - 3);
        if (strcmp("abcdefgh", buf) == 0){
            printf("Check");
        }
      }
    }
  }
}
// [...]
\end{lstlisting}
\end{minipage}

Concerning mutation strategies, \ac{AFL} does not prioritize any part of the input as necessary. It will notice that an input fails to cover a previously discovered branch when mutated, but has to search through all possibilities of mutations on this input in order to come upon one that further explores the program under test.
Taking the example of \cref{lst:comp}, the fuzzer needs to provide \lstinline|"abcd"| as an input to cover line 6, which may fail with a division by zero. \ac{AFL} may generate an input of \lstinline|"abcx"| and recording its coverage as passing the conditionals in line 2-4. But the next inputs generated may be \ac{AFL} \lstinline|"bbcx"| and \lstinline|"1bcx"|. \ac{AFL} will discover, that these do not reach any new coverage but will try the second one even after the first one fails as it doesn't recognize the importance of bytes.

\section{FairFuzz}\label{sec:fairfuzz}
% present solutions of Fairfuzz
FairFuzz reuses the harness and concepts given by \ac{AFL}. Especially instrumentation of code and resulting coverage as vital pieces of information are used to guide it. Adding to it, FairFuzz operates on two concepts added to \ac{AFL} which are ``Targeting Rare Branches'' and ``Masking Mutations''. Both of these concepts are applied in the mutation stage in every iteration, see \cref{fig:iter}.

% Stage selection
FairFuzz expands on \ac{AFL}s concept of coverage and classifies rare and non-rare branches. Rare branches are assumed to be more interesting based on the idea that branches already reached with many inputs will not contain as much unexplored sub-branches as those who are not. Targeting those rare branches specifically therefore may lead to discovering more functionality and reaching more complete code coverage, potentially finding more bugs while doing so.

This classification relies on a computed ``rarity cutoff'' which is computed on every iteration on the input pipeline. To do so, the nearest power of 2 larger than the number of hits for the branch hit the least is used. Every branch hit by fewer than this power of 2 is classified as rare.\cite[Definition 5]{Lemieux2018} This enables FairFuzz to dynamically adjust to new inputs found and new coverage achieved. Replacing the function deciding if an input is worth fuzzing with is own \(HitsRareBranch\), FairFuzz attempts to improve the phase of selection in \ac{AFL}.

\begin{wrapfigure}{R}{0.4\textwidth}
    \centering
    \includegraphics[width=0.39\textwidth]{iter}
    \caption{FairFuzz's logical iterations}\label{fig:iter}
\end{wrapfigure}

% Stage mutation
To improve the phase of mutation, FairFuzz employs masking. This intervenes inside the two mutation stages of \ac{AFL}. Instead of allowing \ac{AFL} to mutate inputs only based on \ac{AFL}s mutation strategies, an additional mask is used to restrict these modifications to specific bytes within an input.
This is done in order to prevent \ac{AFL} from mutating bytes which are essential in reaching the already achieved coverage. FairFuzz determines these masks by selecting an input \( x \) that is arriving at specific branches \( y \), then mutating this input into \( x_{mut} \) and running it through the program under test again. Observations can then be made, wether \( x_{mut} \) still achieves at least branch coverage of \( y \) if not more. If this is the case, the bytes different between \( x \) and \( x_{mut} \) are marked according to the performed mutation as ``Overwriteable'', ``Insertable'' or ``Deletable''.

In the example \cref{lst:comp}, FairFuzz may generate the inputs \lstinline|"abcx"| and \lstinline|"bbcx"| but will then recognize the first byte is not over-writable.
This limits \ac{AFL}s search space to explore only options which may lead to the discovery of more branches and stops it from mutating inputs that in a way not reaching at least current coverage.

% How it is done
Implementation of these two concepts is done in approx. 600 lines of C code added to \ac{AFL}.\cite{Lemieux2018} which enables FairFuzz to reuse the code of \ac{AFL} easily, but also ties it very closely to the same. Installation is done by compiling source code via make.

% Disadvantages of FairFuzz
As a result, FairFuzz not only inherits concepts or advantages, it also results in disadvantages passed on. Though \ref{sec:evaluation} will show that FairFuzz can mitigate some disadvantages by it's introduced mechanisms, but one significant disadvantage shared by both programs is that if some branch is never hit, there is no way to specifically target the one branch. Even with the targeting of rare branches, a branch has to be hit at least once to be considered rare and to start mutating input towards increasing coverage within this branch.\cite[5. Discussion]{Lemieux2018}

Another drawback present is the problem of so called ``magic bytes''\cite{geretto2019qbdi} or ``magic numbers''\cite{Lemieux2018}. This refers to the problem of comparing multiple bytes in the input to a keyword or large number in a single comparison. As \ac{AFL}, FairFuzz would have to mutate the input just right to pass this single comparison and does not gain any more coverage information guiding its mutation towards discovery. This can be seen in \cref{lst:comp} line 7. For the first 4 characters, FairFuzz can gather information about successfully passed comparisons (line 2-5), increasing coverage, but will have to guess \lstinline|"efgh"| ``blindly'', without any coverage feedback.
There are multiple solutions proposed. This include de-optimization steps to break single multi-byte comparisons into multiple single byte ones\cite{Lemieux2018} or the temporary usage of symbolic execution to find such magic bytes\cite{stephens2016driller}.


\section{Evaluation}\label{sec:evaluation}
In \citet{Lemieux2018} FairFuzz is evaluated against \ac{AFL}, \ac{AFL} without deterministic mutations and AFLFast without deterministic stage with the cut-off-exponential exploration strategy.

% Evaluation
% Present problems and Results of FairFuzz
\subsection{Setup}
Unfortunately, there is no information available about the execution environment the tests were done in the paper \citet{Lemieux2018}. Also, no raw data, instrumented applications or performance data is available with the exception of what is visible in the plotted graphs and the numbers presented in \citet{Lemieux2018}. FairFuzz did also not take part in fuzzing competitions like \href{https://sv-comp.sosy-lab.org}{SV-Comp} or other standardized and comparable studies.
Therefore, the experiments cannot be reproduced or verified.

To try \ac{AFL} and FairFuzz more easily, a docker environment was used and is made available with all source code used in this paper.\footnote{\url{https://gitlab.com/Girstenbrei/fuzz}}. This environment can be used to run both fuzzers and includes the example from \cref{lst:comp}.
The containers where run on a virtual machine with 4 Intel(R) Core(TM) i7-5820K CPU @ 3.30GHz cores and 4 gigabyte of RAM. More information about the execution environment is available\footnote{\url{https://gitlab.com/Girstenbrei/fuzz/-/blob/master/cpu.txt}}\footnote{\url{https://gitlab.com/Girstenbrei/fuzz/-/blob/master/ram.txt}}.

\subsection{Results}
Evaluation is done on multiple real-world software tools including tcpdump, xmllint and djpeg, 9 in total. All test runs are 24 hours long and are repeated for 20 times each. To display the shaded 95\% confidence intervals in the plotted data, a Student's distribution with 2.0860 times the standard error is assumed. Due to \ac{AFL}s implementation of the path coverage metric, which dependent on the input order and therefore not stable, branch coverage is reported. This is done with the measurement of basic block transitions reported by \ac{AFL}.

\begin{wrapfigure}{R}{0.4\textwidth}
    \centering
    \includegraphics[width=0.39\textwidth]{tcpdump}
    \caption{Comparing test runs for tcpdump\cite{Lemieux2018}}\label{fig:tcpdump}
\end{wrapfigure}

In the example of tcpdump, as seen in~\cref{fig:tcpdump}, FairFuzz does cover more basic block transitions in a shorter amount of time and is therefore able to potentially discover a larger amount of bugs.

\begin{wrapfigure}{R}{0.4\textwidth}
    \centering
    \includegraphics[width=0.39\textwidth]{djpeg}
    \caption{Comparing test runs for djpeg\cite{Lemieux2018}}\label{fig:djpeg}
\end{wrapfigure}

The case of djpeg is an example as seen in~\cref{fig:djpeg}, where FairFuzz cannot gain a significant lead over all other tested applications within the testing time frame.

Overall, FairFuzz achieves the upper bound in basic block transitions in 8 out of 9 benchmarks. This shows, that FairFuzz can achieve very high branch coverage compared to the other tools tested. This is given 24h execution time and can be a result of input masking, enabling FairFuzz to get past more complicated series of conditions. Another observation is that FairFuzz shows a very steep increase in basic block transitions within the first 2.5 hours. Especially compared to pure \ac{AFL}, FairFuzz can consistently gain more branch coverage in a shorter amount of time. Targeting rare branches contributes to this more effective way of selecting inputs to gain coverage.

As displayed in~\cref{fig:benchmarks}, FairFuzz outperforms the compared tools during the 24h runtime of the experiment consistently.

\begin{wrapfigure}{R}{0.5\textwidth}
    \centering
    \includegraphics[width=0.49\textwidth]{benchmarks}
    \caption{Number of benchmarks on which each technique has the lead in coverage at each hour. A benchmark is counted for multiple techniques if two techniques are tied for the lead.\cite{Lemieux2018}}\label{fig:benchmarks}
\end{wrapfigure}
\todo{cite pictures}

Two examples of this are the ones of xmllint and tcpdump.
As can be seen in~\cref{tab:xmllint}, the success in finding increasingly long sequences belonging to a valid xml document decreases for every tool. But while the compared tooling was only able to discover the 3 character sequence \(<!AT\) at most 3 out of 24 times, FairFuzz was able to reach 11 out of 24. The single run discovering \(<!ATT\) was able to expand this to \(<!ATTLIST\), producing one completely valid xml element name and thereby passing its conditional check. \ac{AFL} was also able to do this once, but out of all inputs produced by \ac{AFL} in this run, only \(0,4\%\) contained \(<!ATTLIST\). This compares to \(12.3\%\) for FairFuzz. The authors of \citet{Lemieux2018} attribute this directly to its mutation mask.
In tcpdump, FairFuzz was able to produce inputs which contain valid packet data for different protocol types. This enables to explore different, package type dependent, paths within tcpdump.

\begin{table}[]
    \centering
    \caption{Number of runs, for each technique, producing an input with the given sequence in 24 hours.\cite{Lemieux2018}}\label{tab:xmllint}
    \begin{tabular}{lcccc}
        \toprule
        sequence        & AFL & FidgetyAFL & AFLFast.new & FairFuzz \\ \midrule
        \textless{}!A   & 7   & 15         & 18          & 17       \\
        \textless{}!AT  & 1   & 2          & 3           & 11       \\
        \textless{}!ATT & 1   & 0          & 0           & 1        \\ \bottomrule
    \end{tabular}
\end{table}

These measurements demonstrate that FairFuzz has a significant advantage over the compared tools. Achieving most complete branch coverage as well as increasing it faster than other tools allow FairFuzz to take the lead in this comparison.

\section{Related Work}
The concepts used by related tools range from different mutation strategies up to symbolic execution.

\ac{AFL} is the most directly related tool to FairFuzz. One shortcoming of \ac{AFL} is it's lack of plugin infrastructure. FairFuzz provides a useful addition some \ac{AFL} users may like to use easily. If not only FairFuzz but other tools also using \ac{AFL} (e.g. Driller, NEUZZ)\todo{provide sources} could be implemented in an opt-in plugin style manor to be used in conjunction with \ac{AFL}, code duplication could be avoided and usability enhanced. It would allow users of \ac{AFL} to try and choose suitable mutation strategies from tools like FairFuzz as well as FairFuzz users benefiting from enhanced versions of \ac{AFL}.

A different tool is NEUZZ. It's main conceptual difference is its reliance on a neural network to produce inputs for a fuzzer. Addressing inefficiencies in purely evolutionary-based input generation, NEUZZ uses data generated by afl to train a feed-forward neural network. This is then able to provide gradient-guided optimizations on how to mutate input.\cite{she2019neuzz}
As FairFuzz does not bring it's own mechanisms on how input is to be mutated when necessary, these two approaches may be combined to provide more information on how to mutate.

VeriFuzz shares with FairFuzz that \ac{AFL} is a part of the software. In contrast to FairFuzz it does not keep track of where to mutate inputs during fuzzing, but addresses the problem of passing complex checks to reach branches. It does so by providing initial tests which are generated by an analysis engine classifying and transforming the program under test. The information generated by the analysis engine are then fed into the fuzzer to reach branch coverage more quickly.\cite{basak2019veri}

As mentioned in \ref{sec:fairfuzz}, one problem concerning both FairFuzz and \ac{AFL} is the one of magic bytes. VeriFuzz already includes aid for the fuzzer to pass these and Drillers concept is working in a similar way. To enable the fuzzing stage, which is done by pure \ac{AFL} in the case of driller, to pass these checks, Driller uses symbolic execution. This is done by letting the fuzzer run a certain amount of time, generating inputs. These inputs are then used as a starting point for symbolic execution, which is used to enhance them to pass difficult checks. These enhanced checks are again pushed to the fuzzer to be again used to explore the program under test. This cycle of fuzzing, symbolic execution and fuzzing again is repeated during execution of driller.\cite{stephens2016driller}

A tool relying completely on symbolic execution is Klee. Trying to solve the same problems, being to find bugs in software, it switches the core concept. This results in Klee not suffering from problems encountered by FairFuzz. One example is passing checks for magic bytes. Klee's symbolic execution engine can pass those by analyzing both sides of the condition without having to try multiple times or make use of a de-optimization step. But changing concepts to symbolic execution also results in disadvantages not inherent to FairFuzz. These include huge complexity introduced by large programs leading to the path explosion problem. Another difference is the inability to execute black-box fuzzing of binaries as symbolic execution requires the source code to be available and readably by the engine.\cite{cadar2008klee}

\section{Discussion}
FairFuzz enhances the ability of \ac{AFL} to find paths in nested comparisons. Especially in cases like xmllint, this increases the change of discovering bugs significantly. This is shown by the paper of \citet{Lemieux2018} convincingly. Also, as clearly stated in it\cite{Lemieux2018}, the problems of magic bytes still prevails.

On a larger note, the paper of \citet{Lemieux2018} reveals two significant problems. The first one is missing repeatability. To verify the results, repeating the experiments and making the data available according to an artifact standard like e.g.\ the \href{https://pldi20.sigplan.org/track/pldi-2020-PLDI-Research-Artifacts}{PLDI Artifact Evaluation process} is necessary.

The second problem is missing extension infrastructure in \ac{AFL}. FairFuzz is implemented directly on top of \ac{AFL} in it's own \href{https://github.com/carolemieux/afl-rb}{forked repository}. Other projects like Driller are also highly dependent on \ac{AFL}, trying to expand its capabilities. \ac{AFL} itself is aware of this, even listing sister projects\cite{Moroz2019}. 
This raises the question of how to integrate functionality into \ac{AFL}. There is machine readable information provided by \ac{AFL} in the format of files on the filesystem, but this does not allow for modifications to \ac{AFL}s behavior.

A plugin infrastructure in \ac{AFL} would enable multiple other projects to be implemented as such plugins and then more easily accessed by users. This would prevent one-off forks like FairFuzz enabling them to integrate with \ac{AFL} more easily and maintainable. At the writing of this paper, \ac{AFL} has released their \href{https://github.com/google/AFL/releases/tag/v2.56b}{version 2.56b} while FairFuzz was implemented on version 2.52b.

\section{Conclusion}
The concepts of rare branches and masking mutations increase the success rate of \ac{AFL} significantly. They lead to faster detection of new and interesting branches and therefore enable \ac{AFL} to find bugs in a more effective manor by limiting number of branches to explore.
Efficiency is increased by limiting \ac{AFL}s ability to mutate necessary parts of the input.
FairFuzzes techniques therefore could be applicable to be included in \ac{AFL} as an option.

\bibliographystyle{plainnat}
\bibliography{bibliography}

\end{document}
