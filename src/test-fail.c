#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>

int main(int argc, char** argv) {

  char buf[8];

  if (read(STDIN_FILENO, buf, 8) < 1) {
    exit(1);
  }

  int divisor = 3;

  if (buf[0] == 'a'){
    if (buf[1] == 'b'){
      if (buf[2] == 'c'){
        int b = 3 / (divisor - 3);
        if (buf[3] == 'd'){
          if (strcmp("abcdefg", buf) == 0){
            printf("Check");
            printf("%d", b);
          }
        }
      }
    }
  }
  exit(0);
}
