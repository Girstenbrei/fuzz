#!/bin/bash

function finish {
    tar -zcvf ./findings-$FUZZER.tar.gz /findings/

}

trap finish EXIT
afl-fuzz -i ./cases -o /findings $1
