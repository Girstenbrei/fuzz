export PDFLATEX := pdflatex -interaction=nonstopmode -aux-directory=build/ -c-style-errors -max-print-line=120 -halt-on-error

pdf:
	$(PDFLATEX) paper

all: pdf
	bibtex build/paper
	$(PDFLATEX) paper
	$(PDFLATEX) paper

clean:
	rm -f paper.pdf
	rm -rf build
	rm -f *.bib.bak

count:
	printf "Characters:\t" | tee wordcount.txt
	pdftotext -enc UTF-8 paper.pdf  - | wc -m | tee -a wordcount.txt
	printf "Words:\t\t" | tee -a wordcount.txt
	pdftotext -enc UTF-8 paper.pdf  - | wc -w | tee -a wordcount.txt

ifndef VERBOSE
.SILENT:
endif